// ==UserScript==
// @name            Fix audio quality in Discord screen capture
// @namespace       xpaw-fix-discord-audio-capture
// @author          xPaw
// @version         1.0.0
// @description     Disable audio processing when broadcasting, force 60 FPS capture
// @match           https://discord.com/*
// @downloadURL     https://gist.github.com/xPaw/65e7abcf5c1db5cfc1114fbc2bb66632/raw/fix-discord-audio-capture.user.js
// @icon            data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24'%3E%3Cpath fill='%235865F2' d='M12 0a12 12 0 1 0 0 24 12 12 0 0 0 0-24Zm0 22a10 10 0 1 1 0-20 10 10 0 0 1 0 20ZM11 6v12a1 1 0 1 1-2 0V6a1 1 0 1 1 2 0ZM7 9v6a1 1 0 1 1-2 0V9a1 1 0 1 1 2 0Zm8-1v8a1 1 0 1 1-2 0V8a1 1 0 1 1 2 0Zm4 3v3a1 1 0 1 1-2 0v-3a1 1 0 1 1 2 0Z'/%3E%3C/svg%3E
// @grant           unsafeWindow
// ==/UserScript==

const originalGetDisplayMedia = unsafeWindow.navigator.mediaDevices.getDisplayMedia;

/** @param {DisplayMediaStreamOptions} constraints */
unsafeWindow.navigator.mediaDevices.getDisplayMedia = function monkeyPatchedGetDisplayMedia(constraints)
{
	if (constraints.audio === true)
	{
		constraints.audio =
		{
			autoGainControl: false,
			echoCancellation: false,
			noiseSuppression: false,
		};
	}

	return originalGetDisplayMedia.apply(this, [constraints]);
};
